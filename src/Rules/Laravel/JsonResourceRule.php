<?php

namespace Hospitable\PHPStan\Rules\Laravel;

use Hospitable\PHPStan\Rules\AbstractClassRule;
use Illuminate\Http\Resources\Json\JsonResource;
use PhpParser\Node;
use PHPStan\Analyser\Scope;

/**
 * @implements \PHPStan\Rules\Rule<\PhpParser\Node\Stmt\Class_>
 */
class JsonResourceRule extends AbstractClassRule
{
    /**
     * @param  \PhpParser\Node\Stmt\Class_  $node
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (! $this->shouldBeProcessed($node)) {
            return [];
        }

        if (! $this->isExtending($node, JsonResource::class)) {
            return [];
        }

        if (! $this->isInNamespace($node, 'App\\Http\\Resources\\')) {
            return [
                $this->error(
                    message: 'JsonResources have to be put in `App\\Http\\Resources\\` namespace.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        if (! $this->hasClassnameSuffix($node, 'Resource')) {
            return [
                $this->error(
                    message: 'JsonResources classnames have to end with `Resource`.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        if (! $this->hasMethod($node, 'toArray', 'array')) {
            return [
                $this->error(
                    message: 'JsonResources have to define a `toArray()` method with return-type of `array`.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        $resourceDoc = $this->getDocComment($node->getProperty('resource'));
        if ($resourceDoc === null) {
            return [
                $this->error(
                    message: 'JsonResources `$resource` property has to use a doc-block.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        if (empty($resourceDoc->getVarTagValues())) {
            return [
                $this->error(
                    message: 'JsonResources `$resource` property doc-block has to define a `@var` tag.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        return [];
    }
}
