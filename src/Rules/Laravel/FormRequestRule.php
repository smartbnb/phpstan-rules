<?php

namespace Hospitable\PHPStan\Rules\Laravel;

use Hospitable\PHPStan\Rules\AbstractClassRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use PhpParser\Node;
use PHPStan\Analyser\Scope;

/**
 * @implements \PHPStan\Rules\Rule<\PhpParser\Node\Stmt\Class_>
 */
class FormRequestRule extends AbstractClassRule
{
    /**
     * @param  \PhpParser\Node\Stmt\Class_  $node
     */
    public function processNode(Node $node, Scope $scope): array
    {
        if (! $this->shouldBeProcessed($node)) {
            return [];
        }

        if (! $this->isExtending($node, FormRequest::class)) {
            return [];
        }

        if (! $this->isInNamespace($node, 'App\\Http\\Requests\\')) {
            return [
                $this->error(
                    message: 'FormRequests have to be put in `App\\Http\\Requests\\` namespace.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        if (! $this->hasClassnameSuffix($node, 'Request')) {
            return [
                $this->error(
                    message: 'FormRequest classnames have to end with `Request`.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        if (! $this->hasMethod($node, 'rules', 'array')) {
            return [
                $this->error(
                    message: 'FormRequests have to define a `rules()` method with return-type of `array`.',
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        return [];

        // ToDo
        $controller = (string) Str::of($fqcn)
            ->replaceFirst('App\\Http\\Requests\\', 'App\\Http\\Controllers\\')
            ->replaceLast('Request', 'Controller');

        if (! class_exists($controller)) {
            return [
                $this->error(
                    message: "FormRequests are expected to match the corresponding Controller naming - expected `{$controller}`.",
                    node: $node,
                    scope: $scope
                ),
            ];
        }

        return [];
    }
}
