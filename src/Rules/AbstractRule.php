<?php

namespace Hospitable\PHPStan\Rules;

use Hospitable\PHPStan\Concerns\BuildsRuleErrors;
use PHPStan\Rules\Rule;

abstract class AbstractRule implements Rule
{
    use BuildsRuleErrors;
}
