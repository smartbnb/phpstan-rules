<?php

namespace Hospitable\PHPStan\Rules;

use Hospitable\PHPStan\Concerns\HasDocComments;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Node\Name\FullyQualified;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\TraitUse;
use PHPStan\Type\ObjectType;

/**
 * @implements \PHPStan\Rules\Rule<\PhpParser\Node\Stmt\Class_>
 */
abstract class AbstractClassRule extends AbstractRule
{
    use HasDocComments;

    public function getNodeType(): string
    {
        return Class_::class;
    }

    protected function shouldBeProcessed(Class_ $node): bool
    {
        if ($node->isAbstract()) {
            return false;
        }

        if ($node->namespacedName === null) {
            return false;
        }

        return true;
    }

    protected function isExtending(Class_ $node, string $extends): bool
    {
        return (new ObjectType($extends))
            ->isSuperTypeOf(new ObjectType((string) $node->namespacedName))
            ->yes();
    }

    protected function isInNamespace(Class_ $node, string $namespace): bool
    {
        return str_starts_with((string) $node->namespacedName, Str::finish($namespace, '\\'));
    }

    protected function hasClassnameSuffix(Class_ $node, string $suffix): bool
    {
        return str_ends_with(class_basename((string) $node->namespacedName), $suffix);
    }

    protected function hasMethod(Class_ $node, string $methodName, string $returnType = null): bool
    {
        $method = $node->getMethod($methodName);

        if ($method === null) {
            return false;
        }

        if (
            $returnType !== null
            && $method->getReturnType()?->name !== $returnType
        ) {
            return false;
        }

        return true;
    }

    protected function usesTrait(Class_ $node, string $traitName): bool
    {
        return Collection::make($node->getTraitUses())
            ->map(fn (TraitUse $use): array => $use->traits)
            ->collapse()
            ->contains(fn (FullyQualified $trait) => $trait->toString() === $traitName);
    }

    protected function implementsInterface(Class_ $node, string $interfaceName): bool
    {
        return Collection::make($node->implements)
            ->contains(fn (FullyQualified $interface) => $interface->toString() === $interfaceName);
    }
}
